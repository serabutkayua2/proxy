<?php
	/* Proxy bootstrap
	 *
	 * Written by Hugo Leisink <hugo@leisink.net>
	 */

	class bootstrap {
		private $config = null;
		private $hostname = null;
		private $request_uri = null;
		private $user_input = "";

		/* Constructor
		 *
		 * INPUT:  array configuration
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($config) {
			$this->config = $config;
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "user_input": return $this->user_input;
				case "hostname": return $this->hostname;
				case "request_uri": return $this->request_uri;
			}

			return null;
		}

		/* Check if hostname exists in list
		 *
		 * INPUT:  string hostname, array list
		 * OUTPUT: boolean hostname exists in list
		 * ERROR:  -
		 */
		private function hostname_in_list($hostname, $list) {
			foreach ($list as $item) {
				if ($item[0] == "*") {
					$item = substr($item, 1);
					if (substr($hostname, -strlen($item)) == $item) {
						return true;
					}
				} else if ($hostname == $item) {
					return true;
				}
			}

			return false;
		}

		/* Check if a login is required
		 *
		 * INPUT:  -
		 * OUTPUT: boolean login required
		 * ERROR:  -
		 */
		private function login_required() {
			if (count($this->config["access_codes"]) == 0) {
				return false;
			}

			if ($this->hostname_in_list($this->hostname, $this->config["no_auth_websites"])) {
				return false;
			} else if (in_array($_SERVER["REMOTE_ADDR"], $this->config["no_auth_clients"])) {
				return false;
			}

			if (in_array($_SESSION["access_code"], $this->config["access_codes"])) {
				return false;
			}

			if (in_array($_POST["access_code"], $this->config["access_codes"])) {
				$_SESSION["access_code"] = $_POST["access_code"];
				$_SERVER["REQUEST_METHOD"] = "GET";
				return false;
			}

			return true;
		}

		/* Execute bootstrap procedure
		 *
		 * INPUT:  -
		 * OUTPUT: integer result
		 * ERROR:  -
		 */
		public function execute() {
			/* Block searchbots
			 */
			$search_bots = array("Googlebot", "bingbot");
			foreach ($search_bots as $bot) {
				if (strpos($_SERVER["HTTP_USER_AGENT"], $bot) !== false) {
					return 403;
				}
			}

			/* Authentication
			 */
			if ($this->login_required()) {
				return LOGIN_REQUIRED;
			}

			/* Local files
			 */
			list($request_uri) = explode("?", $_SERVER["REQUEST_URI"], 2);
			if ($request_uri == "/") {
				return EMPTY_REQUEST_URI;
			}

			if (in_array($request_uri, LOCAL_URLS)) {
				return LOCAL_FILE;
			}

			/* Parse request
			 */
			$uri = ltrim($_SERVER["REQUEST_URI"], "/");
			if (($pos = strpos($uri, "/")) !== false) {
				$this->hostname = substr($uri, 0, $pos);
				$this->request_uri = substr($uri, $pos);
			} else if ($_SERVER["REQUEST_URI"] != "/") {
				header("Location: ".$_SERVER["REQUEST_URI"]."/");
				return 301;
			}

			if ($_SERVER["REQUEST_URI"] != "/") {
				$this->user_input = $_SERVER["REQUEST_URI"];
			}

			/* Access control
			 */
			if (count($this->config["whitelist"]) > 0) {
				if ($this->hostname_in_list($this->hostname, $this->config["whitelist"]) == false) {
					return FORBIDDEN_HOSTNAME;
				}
			}

			if ($this->hostname_in_list($this->hostname, $this->config["blacklist"])) {
				return FORBIDDEN_HOSTNAME;
			}

			return 0;
		}
	}
?>
