<?php
	class view {
		private $config = array();
		private $proxy_hostname = null;
		private $working_dir = null;
		private $output = "";
		private $enabled = true;
		private $mimetypes = array();

		/* Constructor
		 *
		 * INPUT:  array configuration
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($config) {
			$this->config = $config;
			$this->working_dir = str_replace("/libraries", "", __DIR__);

			$this->show_file("header", array(
				"PROXY_HOSTNAME"  => $this->config["proxy_hostname"],
				"PROTOCOL"        => $_SERVER["HTTPS"] == "on" ? "https" : "http",
				"PROTOCOL_LINK"   => $_SERVER["HTTPS"] == "on" ? "http" : "https",
				"SESSION_KEY"     => SESSION_KEY));

			$this->mimetypes = array(
				"css" => "text/css",
				"ico" => "image/x-icon",
				"png" => "image/png",
				"txt" => "text/plain");
		}

		/* Destructor
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __destruct() {
			if ($this->enabled == false) {
				return;
			}

			$this->show_file("footer", array("VERSION" => VERSION));

			print $this->output;
		}

		/* Show file content
		 */
		private function show_file($filename, $replace = null) {
			$output = file_get_contents($this->working_dir."/views/".$filename.".html");
			if ($output == false) {
				return;
			}

			if (is_array($replace)) {
				foreach ($replace as $key => $value) {
					$output = str_replace("{".$key."}", $value, $output);
				}
			}

			$this->output .= $output;
		}

		/* Login form
		 */
		public function show_login_form($message = null) {
			$code = $this->config["proxy_hostname"] == $_SERVER["HTTP_HOST"] ? 401 : 407;
			header("Status: ".$code);

			$data = array(
				"PROTOCOL" => ($_SERVER["HTTPS"] == "on") ? "https" : "http",
				"HOSTNAME" => $_SERVER["HTTP_HOST"],
				"URI"      => $_SERVER["REQUEST_URI"]);
			$this->show_file("login", $data);

			if ($message !== null) {
				$this->show_file("error", array("MESSAGE" => $message));
			}

			$this->show_file("download");
		}

		/* URL form
		 */
		public function show_url_form($url = "", $message = null, $status = null) {
			if ($status !== null) {
				header("Status: ".$status);
			}

			$this->show_file("urlform", array(
				"PROTOCOL"       => $_SERVER["HTTPS"] == "on" ? "https" : "http",
				"PROXY_HOSTNAME" => $this->config["proxy_hostname"],
				"URL"            => $url));
			if ($message !== null) {
				$this->show_file("error", array("MESSAGE" => $message));
			}

			/* Quick links
			 */
			if (count($this->config["quick_links"]) > 0) {
				$links = array();
				foreach ($this->config["quick_links"] as $text => $link) {
					list($prot,, $host, $path) = explode("/", $link, 4);
					if (is_string($text) == false) {
						$text = $host;
					}

					$link = sprintf("%s//%s/%s/%s", $prot, $this->config["proxy_hostname"], $host, $path);

					array_push($links, sprintf("<li><a href=\"%s\">%s</a></li>\n", $link, $text));
				}

				$this->show_file("links", array("LINKS" => implode("\n", $links)));
			}

			/* Show download link and menu
			 */
			$this->show_file("download");
			$this->show_file("menu");
		}

		/* HTTP error message
		 */
		public function http_error($code) {
			$messages = array(
				403 => "Forbidden",
				404 => "Not Found",
				405 => "Unsupported request method",
				500 => "Internal error at remote server");

			if (($message = $messages[$code]) == null) {
				$message = "Unknown error";
			} else {
				header("Status: ".$code);
				$message = sprintf("%d - %s", $code, $message);
			}

			$this->show_file("error", array("MESSAGE" => $message));
			$this->show_file("menu");
		}

		/* Show proxy page
		 */
		public function show_page($page) {
			if (ctype_lower($page) == false) {
				return false;
			}

			$php_file = "views/".$page.".html";
			if (file_exists($php_file) == false) {
				return false;
			}

			ob_start();
			include($php_file);
			$output = ob_get_clean();

			$this->output .= $output;
			$this->show_file("menu");

			return true;
		}
	}
?>
