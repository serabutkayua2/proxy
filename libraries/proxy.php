<?php
	/* Proxy library
	 *
	 * Written by Hugo Leisink <hugo@leisink.net>
	 */

	class proxy extends HTTP {
		const COOKIE_LABEL = "proxy";

		private $config = array();
		private $base_url = null;
		private $hostname = null;
		private $proxy_hostname = null;
		private $ignore_cookies = array();
		private $headers_to_server = array("Accept", "Accept-Charset",
			"Accept-Language", "Referer", "User-Agent", "X-Requested-With",
			"Authorization");
		private $headers_to_client = array("Accept-Ranges", "Cache-Control",
			"Content-Type", "Content-Range", "DNT", "ETag", "Expires",
			"Last-Modified", "Location", "Pragma", "Refresh",
			"WWW-Authenticate");

		/* Constructor
		 *
		 * INPUT:  array configuration, string hostname[, int port]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($config, $hostname, $port = null) {
			$this->config = $config;
			$this->hostname = $hostname;

			parent::__construct($hostname, $port);
		}

		/* Forward HTTP header from browser to server
		 *
		 * INPUT:  string header
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function header_to_server($header) {
			$envkey = "HTTP_".str_replace("-", "_", strtoupper($header));

			if (isset($_SERVER[$envkey]) == false) {
				return;
			}

			$value = $_SERVER[$envkey];
			if ($header == "Referer") {
				$value = str_replace("/".$this->hostname."/", "/", $value);
				$value = str_replace("://".$_SERVER["SERVER_NAME"]."/", "://".$this->hostname."/", $value);
			}

			$this->add_header($header, $value);
		}

		/* Rewrite URL
		 *
		 * INPUT:  string url
		 * OUTPUT: string url
		 * ERROR:  -
		 */
		private function rewrite_url($url) {
			$url = trim($url, " \"'\n");

			if (substr($url, 0, 5) == "data:") {
				return $url;
			}

			if ((substr($url, 0, 2) == "//") && (strlen($url) > 2)) {
				list($host, $path) = explode("/", substr($url, 2), 2);
				$new_url = sprintf("//%s/%s/%s", $this->config["proxy_hostname"], $host, $path);
			} else if (substr($url, 0, 7) == "http://") {
				list($host, $path) = explode("/", substr($url, 7), 2);
				$new_url = sprintf("http://%s/%s/%s", $this->config["proxy_hostname"], $host, $path);
			} else if (substr($url, 0, 8) == "https://") {
				list($host, $path) = explode("/", substr($url, 8), 2);
				$new_url = sprintf("https://%s/%s/%s", $this->config["proxy_hostname"], $host, $path);
			} else if (substr($url, 0, 1) == "/") {
				$new_url = sprintf("/%s%s", $this->hostname, $url);
			} else {
				$new_url = $this->base_url.$url;
			}

			return $new_url;
		}

		/* Fix property value
		 *
		 * INPUT:  string data, string delimeter begin, string delimeter end
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function rewrite_to_proxy($data, $delim_begin, $delim_end) {
			$offset = 0;

			while (($begin = strpos($data, $delim_begin, $offset)) !== false) {
				$begin += strlen($delim_begin);
				if (($end = strpos($data, $delim_end, $begin)) === false) {
					$offset = $begin;
					continue;
				}

				$first = substr($data, 0, $begin);
				$url = substr($data, $begin, $end - $begin);
				$last = substr($data, $end);

				$data = $first.$this->rewrite_url($url).$last;

				$offset = $begin + strlen($new_url) + 1;
			}

			return $data;
		}

		/* Update the cookie path with the hostname
		 */
		private function update_cookie_path($value) {
			$parts = explode(";", $value);
			$has_path = false;

			foreach ($parts as $i => $part) {
				list($key, $value) = explode("=", trim($part));
				if ($value === null) {
					continue;
				}

				$key = trim($key);

				if (strtolower($key) == "domain") {
					$value = $this->config["proxy_hostname"];
				} else if (strtolower($key) == "path") {
					$has_path = true;
					$value = "/".$this->hostname.$value;
				}

				$parts[$i] = $key."=".$value;
			}

			if ($has_path == false) {
				array_push($parts, " Path=/".$this->hostname);
			}

			return implode("; ", $parts);
		}

		/* Ignore cookies for host
		 *
		 * INPUT:  mixed hostname
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function ignore_cookies($host) {
			if (is_array($host) == false) {
				array_push($this->ignore_cookies, $host);
			} else {
				$this->ignore_cookies = array_unique(array_merge($this->ignore_cookies, $host));
			}
		}

		/* Forward request to remote webserver
		 *
		 * INPUT:  string path
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function forward_request($path) {
			/* Proxy self?
			 */
			if ($this->hostname == $this->config["proxy_hostname"]) {
				return LOOPBACK;
			}

			/* Base url and path
			 */
			$this->base_url = "http";
			if ($this->protocol == "tls") {
				$this->base_url .= "s";
			}
			$this->base_url .= "://".$this->config["proxy_hostname"]."/".$this->hostname;
			if (($pos = strrpos($path, "/")) !== false) {
				$this->base_url .= substr($path, 0, $pos + 1);
			} else {
				$this->base_url .= "/";
			}

			/* Forward headers to server
			 */
			foreach ($this->headers_to_server as $header) {
				$this->header_to_server($header);
			}
			#$this->add_header("X-Forwarded-For", $_SERVER["REMOTE_ADDR"]);

			/* POST data
			 */
			$parts = array();
			foreach ($_POST as $key => $value) {
				array_push($parts, urlencode($key)."=".urlencode($value));
			}
			$post = implode("&", $parts);

			/* Cookies from client
			 */
			if (in_array($this->host, $this->ignore_cookies) == false) {
				foreach ($_COOKIE as $name => $value) {
					list($proxy, $hostname, $key) = explode("|", $name, 3);
					if ($proxy != self::COOKIE_LABEL) {
						continue;
					}

					if ($hostname != str_replace(".", "_", $this->hostname)) {
						continue;
					}

					$this->add_cookie($key, $value);
				}
			}

			/* Send request to server
			 */
			switch ($_SERVER["REQUEST_METHOD"]) {
				case "GET":
					$result = $this->GET($path);
					break;
				case "POST":
					$result = $this->POST($path, $post);
					break;
				default:
					return 405;
			}

			/* Abort on error
			 */
			if ($result === false) {
				return CONNECTION_ERROR;
			} else if ($result["status"] >= 500) {
				return $result["status"];
			}

			/* Fix headers
			 */
			if (isset($result["headers"]["location"])) {
				$result["headers"]["location"] = $this->rewrite_url($result["headers"]["location"]);
			}

			/* Fix body
			 */
			if (substr($result["headers"]["content-type"], 0, 9) == "text/html") {
				/* HTML
				 */
				foreach (array("action", "data-src", "href", "src", "srcset") as $property) {
					$result["body"] = $this->rewrite_to_proxy($result["body"], $property.'="', '"');
				}
				$result["body"] = $this->rewrite_to_proxy($result["body"], "url(", ")");
				$result["body"] = $this->rewrite_to_proxy($result["body"], "='", "'");
				$result["body"] = $this->rewrite_to_proxy($result["body"], "( '", "'");
			} else if (substr($result["headers"]["content-type"], 0, 8) == "text/css") {
				/* CSS
				 */
				$result["body"] = $this->rewrite_to_proxy($result["body"], "url(", ")");
				/* Javascript
			} else if ((substr($result["headers"]["content-type"], 0, 15) == "text/javascript") ||
			           (substr($result["headers"]["content-type"], 0, 22) == "application/javascript") ||
			           (substr($result["headers"]["content-type"], 0, 24) == "application/x-javascript")) {
				$result["body"] = $this->rewrite_to_proxy($result["body"], "='", "'");
				$result["body"] = $this->rewrite_to_proxy($result["body"], '+"', '"');
				 */
			}

			/* Add base tag
			 */
			$result["body"] = str_replace("<head>", "<head>\n<base href=\"".$this->base_url."\" />", $result["body"]);

			/* Send result to browser
			 */
			if ($result["status"] != 200) {
				header("Status: ".$result["status"]);
			}

			foreach ($this->headers_to_client as $key) {
				if (($value = $result["headers"][strtolower($key)]) != null) {
					header($key.": ".$value);
				}
			}

			foreach ($result["headers"] as $key => $value) {
				if (strtolower(substr($key, 2)) == "x-") {
					header($key.": ".$value);
				}
			}

			if (in_array($this->host, $this->ignore_cookies) == false) {
				foreach ($result["cookies"] as $key => $value) {
					$value = $this->update_cookie_path($value);
					$key = sprintf("%s|%s|%s", self::COOKIE_LABEL, $this->hostname, $key);
					header(sprintf("Set-Cookie: %s=%s", $key, $value), false);
				}
			}

			print $result["body"];

			if ($result["sock"] !== null) {
				while (($line = fgets($result["sock"])) !== false) {
					print $line;
				}

				fclose($result["sock"]);
				$result["sock"] = null;
			}

			return 0;
		}
	}
?>
